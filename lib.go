package main

// #cgo LDFLAGS: -L. -lrustlib -lm -ldl
/*
extern int RustFunc();
*/
import "C"
import "fmt"

func main() {
	fmt.Println("hi", C.RustFunc())
}
