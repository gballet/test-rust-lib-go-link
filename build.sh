#!/bin/sh

cd rustlib && (RUSTFLAGS="-C link-args=-lc" cargo build --release --no-default-features) && cp ./target/release/librustlib.so .. && cd -
go build
